/**************************************************************
 * ONE LINE TITLE FOR FILE
 *
 * LONGER EXPLANATION OF THE FILE AND ITS PURPOSE
 * MAY BE MORE THAN ONE LINE IN LENGTH.
 *
 * @File:       /folder_name/rebuild-tag-list.js
 * @Project:    INIT-Port3
 * @Author:      <Adrian Gould <Adrian.Gould@nmtafe.wa.edu.au>@nmtafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:   2018
 *
 * History:
 *  v1.0    2018-06-03
 *          Add a summary for this version's changes, additions,
 *          removals, and so on.
 *
 *  v0.0    2017-12-27
 *          Template as provided by Adrian Gould
 **************************************************************/

function ajax_stream($location) {

    if (!window.XMLHttpRequest) {
        alert("Your browser does not support the native XMLHttpRequest object.");
        return;
    }
    try {
        var xhr = new XMLHttpRequest();
        xhr.previous_text = '';

        xhr.onerror = function () {
            alert("[XHR] Fatal Error.");
        };
        xhr.onreadystatechange = function () {
            try {
                if (xhr.readyState == 4) {
                    document.getElementById('BOO').innerHTML = "<h5>Processing: Completed...</h5>";
                }
                else if (xhr.readyState > 2) {
                    var new_response = xhr.responseText.substring(xhr.previous_text.length);
                    console.log(new_response);
                    var result = JSON.parse(new_response);
                    document.getElementById("divProgress").innerHTML = result.message + ' ';
                    document.getElementById("divProgress").setAttribute("aria-valuenow", result.progress + "%");
                    document.getElementById('divProgress').style.width = result.progress + "%";
                    document.getElementById('BOO').innerHTML = "<h5 class='my-2'>Processing: <span class='text-secondary'>" + result.results + "</span></h5>";

                    xhr.previous_text = xhr.responseText;
                }
            }
            catch (e) {
                alert("[XHR STATECHANGE] Exception: " + e);
            }
        };
        xhr.open("GET", $location, true);
        xhr.send();
    }
    catch (e) {
        alert("[XHR REQUEST] Exception: " + e);
    }

}