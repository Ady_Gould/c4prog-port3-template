<?php
define("_CUR_OS", substr(php_uname(), 0, 7) == "Windows" ? "Win" : "_Nix");

if (checkCurrentOS("Win")) {
    $mappath = '/' . str_replace("\\", "/", str_replace("C:\\", "", dirname(__FILE__)));
} else {
    $mappath = dirname(__FILE__);
}

define('APPLICATION_PATH', realpath($mappath . '/../'));
define("BASE_URL", str_replace("\\", "/", '/' . str_replace(ROOT_PATH, '', APPLICATION_PATH . '/')));


// Escape characters within string
function escape($string)
{
    return htmlentities($string, ENT_QUOTES, 'UTF-8');
}

function checkCurrentOS($_OS)
{
    if (strcmp($_OS, _CUR_OS) == 0) {
        return true;
    }
    return false;
}

/**
 * Displays contents of the passed variable in a preformatted HTML tag - use for debugging
 * @param $var
 */
function e($var, $note = '')
{
    echo "<pre>";
    echo $note . ': ';
    var_dump($var);
    echo "</pre>";
}

