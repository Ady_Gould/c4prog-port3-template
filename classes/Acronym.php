<?php

class Acronym
{

    private $_db,
        $_data;

    public function __construct($acronym = null)
    {
        $this->_db = DB::getInstance();
        if ($acronym) {
            $this->find($acronym);
        }
    }

    public function create($fields = array())
    {
        // Replace with the correct code
        return false;
    }

    public function find($acronym = null, $order = "id ASC")
    {
        if (null !== $acronym) {
            if (is_int($acronym)) {
                $condition = ['id', '=', $acronym];
            } else {
                $condition = ['acronym', '=', $acronym];
            }
            $data = $this->_db->get('acronyms', $order, $condition);
        } else {
            $data = [];
        }

        if ($data && $data->count()) {
            $this->_data = $data->results();
            return $data;
        }

        return false;
    }

    public function findAll($acronym = '', $order = "id ASC")
    {
        if ($acronym > '') {
            $data = $this->_db->get('acronyms', $order, array('acronym', 'like', $acronym));
        } else {
            $data = $this->_db->get('acronyms', $order);
        }

        if ($data && $data->count()) {
            $this->_data = $data->results();
            return $data;
        }

        return false;
    }

    public function exists()
    {
        // Replace with the correct code
        return false;
    }

    public function update($fields = array(), $id = null)
    {
        // Replace with the correct code
        return false;
    }

    public function delete($fields = array(), $id = null)
    {
        // Replace with the correct code
        return false;
    }

    public function data()
    {
        // Replace with the correct code
        return false;
    }

    public function results()
    {
        return $this->_db->results();
    }

//    public function findAllWithTag($findTag = null)
//    {
//        if ($findTag) {
//            $field = (is_numeric($findTag)) ? 'id' : 'tags';
//            if ($field == 'id') {
//                $data = $this->_db->get('acronyms', array($field, '=', $findTag));
//            } else {
//                $data = $this->_db->get('acronyms', array($field, 'like', '%' . $findTag . '%'));
//                e($data);
//            }
//            if ($data && $data->count()) {
//                $this->_data = $data;
//                return true;
//            }
//
//        }
//        return false;
//    }
}
