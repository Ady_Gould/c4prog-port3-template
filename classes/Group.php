<?php

class Group
{

    private $_db,
        $_data,
        $_sessionName,
        $_cookieName;

    public function __construct($user = null)
    {
        $this->_db = DB::getInstance();
        $this->_sessionName = Config::get('session/session_name');
        $this->_cookieName = Config::get('remember/cookie_name');
    }

    public function create($fields = array())
    {
        if (!$this->_db->insert('groups', $fields)) {
            throw new Exception('There was a problem creating an account.');
        }
    }

    public function findAll()
    {
        $data = $this->_db->get('groups');

        if ($data && $data->count()) {
            return $data;
        }
        return false;
    }

    public function find($group = null)
    {
        if ($group) {
            $field = (is_numeric($group)) ? 'id' : 'group_name';
            $data = $this->_db->get('groups', array($field, '=', $group));

            if ($data && $data->count()) {
                $this->_data = $data->first();
                return true;
            }
        }
        return false;
    }

    public function exists()
    {
        return (!empty($this->_data)) ? true : false;
    }

    public function update($fields = array(), $id = null)
    {
        if (!$this->_db->update('groups', $id, $fields)) {
            throw new Exception('There was a problem updating');
        }
    }

    public function data()
    {
        return $this->_data;
    }


    public function isPresent($group)
    {
        $results = $this->_db->get('groups', array('group_name', '=', $group));

        return $results;

        return false;
    }


}
