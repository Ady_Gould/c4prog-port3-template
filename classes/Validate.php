<?php

class Validate
{

    private $_passed = false,
        $_errors = array(),
        $_db = null;

    public function __construct()
    {
        $this->_db = DB::getInstance();
    }

    public function check($source, $items = array())
    {
        foreach ($items as $item => $rules) {
            foreach ($rules as $rule => $rule_value) {

                $value = $source[$item];
                $item = escape(trim($item));
                $displayItem = str_replace("_", " ", $item);
                $displayRule = str_replace("_", " ", $rule);
                $displayRuleValue = str_replace("_", " ", $rule_value);

                if ($rule === 'required' && empty($value)) {
                    $this->addError($displayItem, " is required");
                } else if (!empty($value)) {
                    switch (strtolower($rule)) {
                        case 'min':
                            if (strlen($value) < $rule_value) {
                                $this->addError($displayItem, " must be a minimum of {$displayRuleValue} characters.");
                            }
                            break;
                        case 'max':
                            if (strlen($value) > $rule_value) {
                                $this->addError($displayItem, " must be a maximum of {$displayRuleValue} characters.");
                            }
                            break;
                        case 'matches':
                            if ($value != $source[$rule_value]) {
                                $this->addError($displayItem, " must match {$displayRuleValue}.");
                            }
                            break;
                        case 'unique':
                            $check = $this->_db->get($rule_value, "", array($item, '=', $value));
                            if ($check && $check->count() > 0) {
                                $this->addError($displayItem, " already exists.");
                            }
                            break;
                    } // end switch
                } // end if required
            } // end for each rule
        }  // end for each item
        $this->_passed = false;

        if (empty($this->_errors)) {
            $this->_passed = true;
        } //  enf if no errors

        return $this;
    }

    private function addError($item, $error)
    {
        $this->_errors[$item] = $error;
    }

    public function errors()
    {
        return $this->_errors;
    }

    public function passed()
    {
        return $this->_passed;
    }


}
