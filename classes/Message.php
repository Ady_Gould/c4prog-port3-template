<?php

class Message
{
    private static $_msgCount,
        $_messages;

    private
        $_db = null;

    public function __construct()
    {
        $this->_db = DB::getInstance();
        self::$_msgCount = 0;
        self::$_messages = [];
    }

    public function clear()
    {
        self::$_msgCount = 0;
        self::$_messages = [];
        Session::put('messages', '');
    }

    /**
     * Alias for addMessage
     * @param $msg
     */
    public function add($error, $msg = '')
    {
        self::addMessage($error, $msg);

    }

    /**
     * Alias for addMessage
     * @param $msg
     */
    public function addMsg($error, $msg = '')
    {
        self::addMessage($error, $msg);
    }

    private function addMessage($error, $message)
    {
        if ($message === '') {
            self::$_messages[self::$_msgCount++] = $message;
        } else {
            self::$_messages[$error] = $message;
        }
        Session::put('messages', self::$_messages);
    }

    public function messages()
    {
        return Session::get('messages');
    }

    public function messageCount()
    {
        return self::$_msgCount;
    }

    public function count()
    {
        return $this->messageCount();
    }

    public function errorColour($errorLevel)
    {
        switch (strtolower($errorLevel)) {
            case 'danger':
                {
                    $colour = 'danger';
                    break;
                }
            case 'warning':
                {
                    $colour = 'danger';
                    break;
                }
            case 'success':
                {
                    $colour = 'success';
                    break;
                }
            case 'info':
            case 'information':
                {
                    $colour = 'info';
                    break;
                }
            default:
                {
                    $colour = 'dark';
                }
        }
        return $colour;
    }
}
