<?php

class Tag
{

    /**
     * @var DB|null
     */
    private $_db;

    /**
     * @var
     */
    private $_data;

    /**
     * Tag constructor.
     * @param null $tag
     */
    public function __construct($tag = null)
    {
        $this->_db = DB::getInstance();
        if ($tag) {
            $this->find($tag);
        }
    }

    /**
     * @param array $fields
     * @throws Exception
     */
    public function create($fields = array())
    {
        if (!$this->_db->insert('tags', $fields)) {
            throw new Exception('There was a problem saving a tag.');
        }
    }

    /**
     * @param null $tag
     * @return bool
     */
    public function find($tag = null, $order = "id ASC")
    {
        if ($tag) {
            $field = (is_numeric($tag)) ? 'id' : 'tag_name';
            $data = $this->_db->get('tags', $order, array($field, '=', $tag));

            if ($data && $data->count()) {
                $this->_data = $data->first();
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $tag
     * @return bool|DB
     */
    public function findAll($tag = '', $order = "id ASC")
    {
        if ($tag > '') {
            $data = $this->_db->get('tags', $order, array('tag_name', 'like', $tag));
        } else {
            $data = $this->_db->get('tags', $order);
        }

        if ($data && $data->count()) {
            $this->_data = $data->results();
            return $data;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function exists()
    {
        return (!empty($this->_data)) ? true : false;
    }

    /**
     * @param array $fields
     * @param null $id
     * @throws Exception
     */
    public function update($fields = array(), $id = null)
    {
        if (!$this->_db->update('tags', $id, $fields)) {
            throw new Exception('There was a problem updating');
        }
    }

    /**
     * Removes the record that matches the id
     *
     * @param array $field
     * @param null $id
     * @throws Exception
     */
    public function delete($field = null, $id = null)
    {
        if ($field == null || $id == null) {
            return false;
        }
        if (!$this->_db->delete('tags', [$field, '=', $id])) {
            throw new Exception('There was a problem deleting');
        }
    }

    /**
     * Returns the data retrieved from the database | boolean
     *
     * @return mixed
     */
    public function data()
    {
        return $this->_data;
    }

    public function results()
    {
        return $this->_db->results();
    }

    /**
     * @param null $findTag
     * @return bool
     */
    public function findAllWithTag($findTag = null, $order = "id ASC")
    {
        if ($findTag) {
            $field = (is_numeric($findTag)) ? 'id' : 'tags';
            if ($field == 'id') {
                $acronymData = $this->_db->get('acronyms', $order, array($field, '=', $findTag));
            } else {
                $acronymData = $this->_db->get('acronyms', $order, array($field, 'like', $findTag));
            }
            if ($acronymData && $acronymData->count()) {
                $this->_data = $acronymData;
                return $this->results();
            }

        }
        return false;
    }

    public function count()
    {
        return $this->_db->count();
    }


} // end class Tag
