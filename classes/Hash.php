<?php

class Hash
{

    public static function make($string, $salt = '')
    {
        return hash('sha256', $string . $salt);
    }


    public static function salt($length)
    {
        if (!function_exists('mcrypt_create_iv')) {
            return random_bytes($length);
        }
        return mcrypt_create_iv($length);
    }

    public static function unique()
    {
        return self::make(uniqid('', true));
    }


}
