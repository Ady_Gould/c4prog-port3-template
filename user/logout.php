<?php
require_once __DIR__ . '/../core/init.php';

$user = new User();
$user->logout();
$messages = new Message();

Redirect::to('../index.php');
