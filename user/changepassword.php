<?php
require_once __DIR__ . '/../core/init.php';
Session::put('title', 'Change Password | User');

$user = new User();
$messages = new Message();

if (!$user->isLoggedIn()) {
    Redirect::to('index.php');
}

if (Input::exists()) {
    if (Token::check(Input::get('token'))) {

        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'password_current' => array(
                'required' => true,
                'min' => 6
            ),
            'password_new' => array(
                'required' => true,
                'min' => 6
            ),
            'password_new_again' => array(
                'required' => true,
                'min' => 6,
                'matches' => 'password_new'
            )
        ));

        if ($validation->passed()) {

            if (Hash::make(Input::get('password_current'), $user->data()->salt) !== $user->data()->password) {
                echo 'Your current password is wrong';
            } else {
                $salt = Hash::salt(32);
                $user->update(array(
                    'password' => Hash::make(Input::get('password_new'), $salt),
                    'salt' => $salt
                ));

                Session::flash('home', 'Your password has been changed');
                $messages->add('home', 'Your password has been changed');
                Redirect::to('index.php');

            }

        } else {
            foreach ($validation->errors() as $error => $msg) {
                $messages->add($error, $msg);
            } // end foreach
        }// end if password validated

    } // end token check
} else {
    $messages->clear();
}

require_once DOC_ROOT . 'templates/header.php';
?>

    <div class="jumbotron">
        <div class="row">
            <div class="col-10">
                <h1>Change password for
                    "<?= $user->isLoggedIn() ? escape($user->data()->given_name) : ''; ?>"
                </h1>
                <p>This version by: <em>YOUR NAME HERE</em></p></div>
            <div class="col-2">
                <?php
                if ($user->isLoggedIn()) {
                    if ($user->hasPermission(['admin'])) {
                        echo '<p><i class="fas fa-user-secret fa-5x text-danger"></i></p>';
                    } elseif ($user->hasPermission(['moderator'])) {
                        echo '<p><i class="fas fa-user fa-5x text-success"></i></p>';
                    } else {
                        echo '<p><i class="fas fa-user fa-5x text-secondary"></i></p>';
                    } // end if
                } // end logged in
                ?>
            </div>
        </div>
    </div>

<?php
$messages = $messages->messages();
if ($messages > '' && $messages->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-danger alert-dismissible fade show" role="alert">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i><strong>  <?= $error; ?></strong></span>
            <span class="col-9"><?= $message; ?></span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </p>
        <?php
    } // end foreach
} // end if messages
?>

    <form action="" method="post">

        <div class="form-group">
            <label for="password_current">Current Password</label>
            <input class="form-control" type="password" name="password_current" id="password_current"/>
        </div>

        <div class="form-group">
            <label for="password_new">New Password</label>
            <input class="form-control" type="password" name="password_new" id="password_new"/>
        </div>

        <div class="form-group">
            <label for="password_new_again">New Password Again</label>
            <input class="form-control" type="password" name="password_new_again" id="password_new_again"/>
        </div>

        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>"/>
        <button class="btn btn-primary" type="submit" value="Register">Change Password</button>

    </form>

<?php
require_once DOC_ROOT . "templates/footer.php";