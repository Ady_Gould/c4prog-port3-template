<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Home | User');

$user = new User();
$messages = new Message();


require_once DOC_ROOT . 'templates/header.php';

?>
    <div class="jumbotron bg-primary text-light">
        <div class="row">
            <div class="col-9">
                <h1>All Users</h1>
                <p>This version by: <em>Adrian Gould</em></p>
            </div>
            <div class="col-2 text-right">
                <p>
                    <i class="fas fa-user fa-7x"></i>
                </p>
            </div>
        </div>
    </div>
<?php

if (!$user->isLoggedIn()) {
    echo '<p>You need to <a href="' . BASE_URL . 'user/login.php">login</a> ';
    echo 'or <a href="' . BASE_URL . 'user/register.php">register</a></p>';
}

$allUsers = new User();
$allUsers->findAll();

?>
    <div class="row">
        <div class="col">
            There are <?= count($allUsers->data()); ?> users in the system.
        </div>
    </div>
<?php
require_once DOC_ROOT . 'templates/footer.php';