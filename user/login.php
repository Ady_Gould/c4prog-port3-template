<?php
require_once __DIR__ . '/../core/init.php';
Session::put('title', 'Log in');
$messages = new Message();

if (Input::exists()) {
    if (Token::check(Input::get('token'))) {

        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'username' => array('required' => true),
            'password' => array('required' => true),
        ));

        if ($validation->passed()) {
            $user = new User();

            $remember = (Input::get('remember') === 'on') ? true : false;
            $login = $user->login(Input::get('username'), Input::get('password'), $remember);

            if ($login) {
                Redirect::to('../index.php');
            } else {
                $messages->add('login-fail', 'Sorry, logging in failed');

            }

        } else {
            foreach ($validation->errors() as $error => $msg) {
                $messages->add($error, $msg);
            }
        }

    }
} else {
    $messages->clear();
}

require_once DOC_ROOT . 'templates/header.php';

?>
    <div class="jumbotron">
        <h1>Welcome to Portfolio 3</h1>
        <p>This version by: <em>YOUR NAME HERE</em></p>
    </div>
<?php

if ($messages > '' && $messages->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-danger alert-dismissible">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i><strong>  <?= $error; ?></strong></span>
            <span class="col-9"><?= $message; ?></span>
        </p>
        <?php
    } // end foreach
} // end if messages
?>


    <form action="" method="post">
        <div class="form-group">
            <label for="username">Username</label>
            <input class="form-control" type="text" name="username" id="username" autocomplete="off"/>
        </div>

        <div class="form-group">
            <label for="password">Password</label>
            <input class="form-control" type="password" name="password" id="password" autocomplete="off"/>
        </div>

        <div class="form-group form-check">
            <input class="form-check-input" type="checkbox" name="remember" id="remember"/>
            <label for="remember" class="form-check-label">
                Remember Me
            </label>
        </div>

        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>"/>
        <button type="submit" value="Log In" class="btn btn-primary">Log In</button>

    </form>

<?php
require_once DOC_ROOT . 'templates/footer.php';