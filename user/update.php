<?php
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Update Profile | User');

$user = new User();
$messages = new Message();

if (!$user->isLoggedIn()) {
    Redirect::to('index.php');
}

if (Input::exists()) {
    if (Token::check(Input::get('token'))) {

        $validate = new Validate();
        $validation = $validate->check($_POST,
            [
                'last_name' =>
                    [
                        'required' => true,
                        'min' => 2,
                        'max' => 32
                    ],
                'given_name' =>
                    [
                        'required' => true,
                        'min' => 2,
                        'max' => 32
                    ],
            ]
        );

        if ($validation->passed()) {

            try {
                $user->update(array(
                    'given_name' => Input::get('given_name'),
                    'last_name' => Input::get('last_name')
                ));
                Session::flash('home', 'Your details have been updated');
                Redirect::to('index.php');
            } catch (Exception $e) {
                die ($e->getMessage());
            }

        } else {
            foreach ($validation->errors() as $error) {
                $messages->add($error, $msg);
            }
        }

    } else {
        $messages->clear();
    }

}
require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron">
        <div class="row">
            <div class="col-10">
                <h1>Update your details
                    <?= $user->isLoggedIn() ? ', ' . escape($user->data()->given_name) : ''; ?>
                </h1>
                <p>This version by: <em>YOUR NAME HERE</em></p></div>
            <div class="col-2">
                <?php
                if ($user->isLoggedIn()) {
                    if ($user->hasPermission(['admin'])) {
                        echo '<p><i class="fas fa-user-secret fa-5x text-danger"></i></p>';
                    } elseif ($user->hasPermission(['moderator'])) {
                        echo '<p><i class="fas fa-user fa-5x text-success"></i></p>';
                    } else {
                        echo '<p><i class="fas fa-user fa-5x text-secondary"></i></p>';
                    } // end if
                } // end logged in
                ?>
            </div>
        </div>
    </div>


<?php
if ($messages > '' && $messages->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-danger alert-dismissible fade show" role="alert">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i><strong>  <?= $error; ?></strong></span>
            <span class="col-9"><?= $message; ?></span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </p>
        <?php
    } // end foreach
} // end if messages
?>


    <form action="" method="post">

        <div class="form-group">
            <label for="given_name">User Name</label>
            <input class="form-control" type="text" name="not_used" id="not_used" disabled
                   value="<?php echo escape($user->data()->user_name); ?>"/>
        </div>

        <div class="form-group">
            <label for="given_name">Given Name</label>
            <input class="form-control" type="text" name="given_name" id="given_name"
                   value="<?php echo escape($user->data()->given_name); ?>"/>
        </div>

        <div class="form-group">
            <label for="last_name">Last Name</label>
            <input class="form-control" type="text" name="last_name" id="last_name"
                   value="<?php echo escape($user->data()->last_name); ?>"/>
        </div>

        <button type="submit" value="Update" class="btn btn-primary">Update</button>
        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>"/>

    </form>

<?php

require_once DOC_ROOT . 'templates/footer.php';
