<?php
require_once __DIR__ . '/../core/init.php';
Session::put('title', 'Retrieve All | User');

Session::put('Profile', 'Home');

$user = new User();
$messages = new Message();

if (!$user->isLoggedIn() || !$user->hasPermission(['moderator', 'admin'])) {
    Redirect::to(BASE_URL . 'index.php');
}

$findMe = '';
if (isset($_GET) && isset($_GET['search']) && !isset($_GET['clear'])) {
    $findMe = trim($_GET['search']);
}

$allUsers = new User();
$allUsers->findAll($findMe);

$data = [];
if ($allUsers->count() > 0) {
    if (is_array($allUsers->data())) {
        $data = $allUsers->data();
    } else {
        $data = [$allUsers->data()];
    }
}
require_once DOC_ROOT . 'templates/header.php';

?>
    <div class="jumbotron bg-primary text-light">
        <div class="row">
            <div class="col-9">
                <h1>Search Users</h1>
                <p>This version by: <em>Adrian Gould</em></p>
            </div>
            <div class="col-2 text-right">
                <p><i class="fas fa-user fa-7x"></i></p>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search"
                       id="search" name="search" value="<?= $findMe; ?>">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search"></i>
                    Search
                </button>
                <button class="btn btn-outline-warning m-2 my-sm-0" type="submit" name="clear" value="clear">
                    <i class="fa fa-backward"></i> Clear
                </button>
            </form>
        </div>

        <h4 class="col text-right">
            We found <?= ($allUsers->data() ? count($data) : '0'); ?> users
        </h4>
    </div>

    <div class="card-columns">
        <?php
        foreach ($data as $datum) {
            ?>
            <div class="card text-white bg-secondary" style="max-width: 22rem;">
                <div class="card-header">
                    <h5 class="card-title"><?= $datum->given_name; ?> <?= $datum->last_name; ?></h5>
                    <h6 class="card-subtitle text-light text-right"><?= $datum->user_name; ?></h6>
                </div>
                <div class="card-body">
                    <p class="card-img">
                        <?php
                        if ($datum->user_picture !== 'blank.png') {
                            ?>
                            <img src="<?= BASE_URL . 'assets/images/' . $datum->user_picture; ?>"
                                 alt="Picture of <?= $datum->given_name; ?>"
                                 class="img-fluid img-thumbnail rounded-circle">
                            <?php
                        } else {
                            echo '<p class="text-center"><i class="fas fa-user fa-10x text-center text-secondary img-thumbnail"></i></p>';
                        }
                        ?>
                    </p>
                </div>
                <div class="card-footer">
                    Joined: <?= $datum->created; ?>
                </div>
            </div>
            <?php
        } // end foreach
        ?>
    </div>
<?php
require_once DOC_ROOT . 'templates/footer.php';