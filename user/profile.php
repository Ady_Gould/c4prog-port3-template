<?php
require_once __DIR__ . '/../core/init.php';
Session::put('title', 'Profile | User');

Session::put('Profile', 'Home');
$messages = new Message();


if (!$username = Input::get('user')) {
    Redirect::to('index.php');
} else {
    $user = new User($username);
    if (!$user->exists()) {
        Redirect::to(404);
    } else {
        $data = $user->data();
    }
}
require_once DOC_ROOT . 'templates/header.php';

?>

    <div class="jumbotron">
        <div class="row">
            <div class="col-10">
                <h1>Profile Details for
                    "<?= escape($user->data()->given_name); ?>"
                </h1>
                <p>This version by: <em>YOUR NAME HERE</em></p></div>
            <div class="col-2">
                <?php
                if ($user->hasPermission(['admin'])) {
                    echo '<p><i class="fas fa-user-secret fa-5x text-danger"></i></p>';
                } elseif ($user->hasPermission(['moderator'])) {
                    echo '<p><i class="fas fa-user-astronaut fa-5x text-success"></i></p>';
                } else {
                    echo '<p><i class="fas fa-user fa-5x text-secondary"></i></p>';
                } // end if
                ?>
            </div>
        </div>
    </div>

    <h3 class="row">
        <div class="col-3">User:</div>
        <div class="col-9"><?php echo escape($data->user_name); ?></div>
    </h3>
    <h4 class="row">
        <div class="col-3">Full Name:</div>
        <div class="col-9"><?php echo escape($data->given_name); ?><?php echo escape($data->last_name); ?></div>
    </h4>
    <h5 class="row">
        <div class="col-3">Role:</div>
        <div class="col-9">
            <?php
            if ($user->hasPermission(['admin'])) {
                echo '<p>Administrator <i class="fas fa-user-secret text-danger"></i></p>';
            } elseif ($user->hasPermission(['moderator'])) {
                echo '<p>Moderator <i class="fas fa-user text-success"></i> Moderator</p>';
            } else {
                echo '<p>Standard User <i class="fas fa-user text-secondary"></i></p>';
            } // end if
            ?>
        </div>
    </h5>
    <div class="row">
        <div class="col-3">Joined:</div>
        <div class="col-9"><?php echo escape($data->created); ?></div>
    </div>

<?php

require_once DOC_ROOT . 'templates/footer.php';