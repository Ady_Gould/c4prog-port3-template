<?php
require_once __DIR__ . '/../core/init.php';

Session::put('Register', 'home');
$messages = new Message();

if (Input::exists()) {
    if (Token::check(Input::get('token'))) {

        $messages->clear();

        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'username' => array(
                'required' => true,
                'min' => 2,
                'max' => 24,
                'unique' => 'users'
            ),
            'password' => array(
                'required' => true,
                'min' => 6
            ),
            'password_again' => array(
                'required' => true,
                'matches' => 'password'
            ),
            'givenname' => array(
                'required' => false,
                'min' => 0,
                'max' => 64
            ),
            'lastname' => array(
                'required' => true,
                'min' => 2,
                'max' => 64
            )
        ));

        if ($validation->passed()) {
            $user = new User();
            $salt = Hash::salt(32);

            try {
                $user->create(array(
                    'user_name' => Input::get('username'),
                    'user_password' => Hash::make(Input::get('password'), $salt),
                    'salt' => $salt,
                    'given_name' => Input::get('givenname'),
                    'last_name' => Input::get('lastname'),
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s'),
                    'user_group' => 1
                ));

                $messages->add('home', 'You have been registered and can now log in!');
                Redirect::to('index.php');
            } catch (Exception $e) {
                die($e->getMessage());
            }
        } else {
            foreach ($validation->errors() as $error => $msg) {
                $messages->add($error, $msg);
            }
        }


    }
} else {
    $messages->clear();
}

require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron">
        <div class="row">
            <div class="col-10">
                <h1>Register a New User</h1>
                <p>This version by: <em>YOUR NAME HERE</em></p></div>
            <div class="col-2">

            </div>
        </div>
    </div>
<?php

$messages = $messages->messages();
if ($messages > '' && $messages->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-danger alert-dismissible fade show" role="alert">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i><strong>  <?= $error; ?></strong></span>
            <span class="col-9"><?= $message; ?></span>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </p>
        <?php
    } // end foreach
} // end if messages
?>
    <form action="register.php" method="post">
        <div class="form-group">
            <label for="username">User name</label>
            <input class="form-control" type="text" name="username" id="username"
                   value="<?php echo escape(Input::get('user_name')); ?>"
                   autocomplete="off"/>
        </div>

        <div class="form-group">
            <label for="givenname">Given Name</label>
            <input class="form-control" type="text" name="givenname"
                   value="<?php echo escape(Input::get('givenname')); ?>" id="givenname"/>
        </div>

        <div class="form-group">
            <label for="lastname">Last Name</label>
            <input class="form-control" type="text" name="lastname"
                   value="<?php echo escape(Input::get('lastname')); ?>" id="lastname"/>
        </div>

        <div class="form-group">
            <label for="password">Choose a password</label>
            <input class="form-control" type="password" name="password" id="password"/>
        </div>

        <div class="form-group">
            <label for="password_again">Enter your password again</label>
            <input class="form-control" type="password" name="password_again" id="password_again"/>
        </div>

        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>"/>
        <button class="btn btn-primary" type="submit" value="Register">Register Me</button>

    </form>

<?php

require_once DOC_ROOT . 'templates/footer.php';