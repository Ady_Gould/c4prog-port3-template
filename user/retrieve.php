<?php
require_once __DIR__ . '/../core/init.php';
Session::put('title', 'Retrieve All | User');

Session::put('Profile', 'Home');

$user = new User();
$messages = new Message();

if (!$user->isLoggedIn() || !$user->hasPermission(['moderator', 'admin'])) {
    Redirect::to(BASE_URL . 'index.php');
}

$allUsers = new User();
$allUsers->findAll();

require_once DOC_ROOT . 'templates/header.php';

?>
    <div class="jumbotron bg-primary text-light">
        <div class="row">
            <div class="col-9">
                <h1>All Users</h1>
                <p>This version by: <em>Adrian Gould</em></p>
            </div>
            <div class="col-2 text-right">
                <p><i class="fas fa-user fa-7x"></i></p>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <h4 class="col">
            There are <?= count($allUsers->data()); ?> users in the system.
        </h4>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-columns">
                <?php
                $row = 0;
                foreach ($allUsers->data() as $datum) {
                    ?>
                    <div class="card text-white bg-secondary" style="max-width: 22rem;">
                        <div class="card-header">
                            <div class="row">
                                <h5 class="card-title col-9"><?= $datum->given_name; ?> <?= $datum->last_name; ?></h5>
                                <h6 class="card-subtitle text-light text-right col-3">
                                    <?php
                                    if ($datum->user_group == 'admin') {
                                        echo '<p><i class="fas fa-user-secret fa-2x mt-1"></i></p>';
                                    } elseif ($datum->user_group == 'moderator') {
                                        echo '<p><i class="fas fa-user-astronaut fa-2x mt-1"></i></p>';
                                    } else {
                                        echo '<p><i class="fas fa-user fa-2x mt-1"></i></p>';
                                    } // end if
                                    ?></h6>
                            </div>
                        </div>
                        <div class="card-body">
                            <p class="card-img">
                                <?php
                                if ($datum->user_picture !== 'blank.png') {
                                    ?>
                                    <img src="<?= BASE_URL . 'assets/images/' . $datum->user_picture; ?>"
                                         alt="Picture of <?= $datum->given_name; ?>"
                                         class="img-fluid img-thumbnail rounded-circle">
                                    <?php
                                } else {
                                    echo '<p class="text-center"><i class="fas fa-user fa-10x text-center text-secondary img-thumbnail"></i></p>';

                                }
                                ?>
                        </div>
                        <p class="card-footer small">
                            Joined: <?= $datum->created; ?>
                        </p>
                    </div>
                    <?php
                    $row++;
                } // end foreach

                ?>
            </div>
        </div>
    </div>
<?php
require_once DOC_ROOT . 'templates/footer.php';