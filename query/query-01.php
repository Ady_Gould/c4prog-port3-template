<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';
Session::put('title', 'Query 01');
require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron">
        <div class="row">
            <div class="col-10">
                <h1>Welcome to Portfolio 3</h1>
                <h3>Answer for <?= Session::get('title'); ?></h3>
                <p>This version by: <em>YOUR NAME HERE</em></p>
            </div>
            <div class="col-2">
                <p><i class="fas fa-database fa-8x text-danger"></i></p>
            </div>
        </div>
    </div>
<?php

$db = DB::getInstance();

//TODO: Write your SQL in the $sql variable below. A sample Query (NOT THE ANSWER) is shown.
$sql = 'SELECT * FROM acronyms WHERE id IN (1, 3, 6, 9)';

// DO NOT CHANGE ANYTHING BELOW THIS LINE
$query = $db->query($sql);
$results = $query->results();
?>
    <h5>Query: <code><?= $sql; ?></code></h5>
    <table class="table">
        <thead class="bg-dark text-light">
        <th></th>
        <th>Acronym</th>
        <th>Added</th>
        <th>Tags</th>
        </thead>
        <tbody>
        <?php
        $row = 0;
        foreach ($results as $key => $result) {
            $row++;
            ?>
            <tr>
                <td><?= $row ?></td>
                <td><?= $result->acronym ?></td>
                <td><?= $result->created ?></td>
                <td><?= $result->tags ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="4" class="bg-dark text-light">
                Total records: <?= count($results); ?>
            </th>
        </tr>
        </tfoot>
    </table>
<?php
require_once DOC_ROOT . 'templates/footer.php';