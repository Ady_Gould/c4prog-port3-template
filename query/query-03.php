<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';
Session::put('title', 'Query 03');
require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron">
        <div class="row">
            <div class="col-10">
                <h1>Welcome to Portfolio 3</h1>
                <h3>Answer for <?= Session::get('title'); ?></h3>
                <p>This version by: <em>YOUR NAME HERE</em></p>
            </div>
            <div class="col-2">
                <p><i class="fas fa-database fa-8x text-primary"></i></p>
            </div>
        </div>
    </div>
<?php

$db = DB::getInstance();

//TODO: Write your SQL in the $sql variable below. A sample Query (NOT THE ANSWER) is shown.
$sql = 'SELECT * FROM users WHERE user_name LIKE "A%"';

// DO NOT CHANGE ANYTHING BELOW THIS LINE
$query = $db->query($sql);
$results = $query->results();
?>
    <h5>Query: <code><?= $sql; ?></code></h5>
    <table class="table">
        <thead class="bg-dark text-light">
        <th>ID</th>
        <th>Acronym</th>
        <th>Given Name</th>
        <th>Last Name</th>
        <th>Acronym Added</th>
        </thead>
        <tbody>
        <?php
        foreach ($results as $key => $result) {
            ?>
            <tr>
                <td><?= $result->id ?></td>
                <td><?= $result->user_name ?></td>
                <td><?= $result->given_name ?></td>
                <td><?= $result->last_name ?></td>
                <td><?= $result->created ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="5" class="bg-dark text-light">
                Total records: <?= count($results); ?>
            </th>
        </tr>
        </tfoot>
    </table>
<?php
require_once DOC_ROOT . 'templates/footer.php';