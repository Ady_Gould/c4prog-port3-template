<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Home | Tag');

// if form not posted then redirect to the index...
if (!Input::exists() || Input::get('detail') == '' || Input::get('findThis') == '') {
    header('Location: index.php');
}
$user = new User();
$tags = new Tag();

$findMe = Input::get('findThis');

$tags->findAll($findMe, "tag_name ASC");
$numFound = $tags->count();


// make sure the data returned is in an array for processing in the page
$data = [];
if ($tags->count() > 0) {
    if (is_array($tags->data())) {
        $data = $tags->data();
    } else {
        $data = [$tags->data()];
    }
}

$acronymsWithTag = $tags->findAllWithTag($findMe);
if ($tags->count() > 0) {
    if (!is_array($acronymsWithTag)) {
        $acronymsWithTag = [$tags->data()];
    }
}
require_once DOC_ROOT . 'templates/header.php';

?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Tag Details</h1>
                <p class="small">This version by: <em>Adrian Gould</em></p></div>
            <div class="col-2">
                <a href="<?= BASE_URL ?>tag/" class="text-light nav-link"><i
                            class="fas fa-tag fa-7x mx-1 text-light"></i></a>
            </div>
        </div>
    </div>
<?php

?>

    <div class="row mb-3">
        <div class="col">
            <h2><?= $findMe; ?></h2>
        </div>

        <h5 class="col text-right">
            <?= $findMe; ?> was found <?= (is_array($acronymsWithTag) ? count($acronymsWithTag) : 0); ?> times
        </h5>
    </div>

<?php
if (is_array($acronymsWithTag)) {
    ?>
    <p class="col-12 alert alert-info">NB: These links are expecting a retrieve.php in the acronym folder</p>
    <div class="row">
        <?php
        foreach ($acronymsWithTag as $taggedAcronym) {
            ?>
            <form class="form-inline my-2 my-lg-0 col col-3" method="post"
                  action="<?= BASE_URL ?>acronym/retrieve.php">
                <input class="form-control mr-sm-2" type="hidden" id="acronymn-<?= $taggedAcronym->acronym; ?>"
                       name="acronymn"
                       value="<?= $taggedAcronym->acronym; ?>">
                <input class="form-control mr-sm-2" type="hidden" id="id-<?= $taggedAcronym->id; ?>"
                       name="acronym_id"
                       value="<?= $taggedAcronym->id; ?>">
                <button class="btn btn-success text-light m-1 col"><?= $taggedAcronym->acronym; ?></button>
            </form>
            <?php
        } // end for each acronym
        ?>
    </div>
    <?php
} // end if acronyms with data is an array

require_once DOC_ROOT . 'templates/footer.php';