<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Home | Tag');

$user = new User();
$tags = new Tag();
$messages = new Message();

$findMe = '';
if (isset($_POST) && isset($_POST['findThis']) && !isset($_POST['clear'])) {
    $findMe = trim($_POST['findThis']);
}

$tags->findAll($findMe, "tag_name ASC");
$numFound = $tags->count();


// make sure the data returned is in an array for processing in the page
$data = [];
if ($tags->count() > 0) {
    if (is_array($tags->data())) {
        $data = $tags->data();
    } else {
        $data = [$tags->data()];
    }
}

require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Tags</h1>
                <p class="lead">List, Search and Edit</p>
                <p>
                    Select action by clicking on the appropriate icon:
                    <i class="fa fa-info-circle mx-1"></i> Details
                    <i class="fa fa-pencil-alt mx-1"></i> Edit
                    <i class="fa fa-minus-circle mx-1"></i> Delete
                </p>
                <p><a href="<?= BASE_URL; ?>tag/create.php" class="btn btn-outline-light my-1">Add New</a></p>
                <p class="small">This version by: <em>Adrian Gould</em></p></div>
            <div class="col-2">
                <a href="<?= BASE_URL ?>tag/" class="text-light nav-link"><i
                            class="fas fa-tag fa-7x mx-1 text-light"></i></a>
            </div>
        </div>
    </div>

<?php
if ($messages && $messages->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-<?= $message->errorColour($error); ?> alert-dismissible">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i>
                <strong><?= $error; ?></strong>
            </span>
            <span class="col-9"><?= $message; ?></span>
        </p>
        <?php
    } // end foreach
} // end if messages
$messages->clear();
?>


    <table class="table table-hover">
        <thead>
        <tr>
            <td class="border-0">
                <form class="form-inline my-2 my-lg-0" method="post">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search"
                           id="findThis" name="findThis" value="<?= $findMe; ?>">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search" value="search">
                        <i class="fa fa-search"></i> Search
                    </button>
                    <button class="btn btn-outline-warning m-2 my-sm-0" type="submit" name="clear" value="clear">
                        <i class="fa fa-backward"></i> Clear
                    </button>
                </form>
            </td>
            <td class="border-0">
                We found <?= $numFound; ?> tags
            </td>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($data as $tag) {
            ?>
            <tr>
                <td>
                    <form method="post" action="retrieve.php" class="form-inline" name="details">
                        <input type="hidden" name="findThis" value="<?= $tag->tag_name; ?>"/>
                        <button class="btn btn-link text-dark text-left col" type="submit" name="detail" value="detail">
                            <?= $tag->tag_name; ?>
                        </button>
                    </form>
                </td>
                <td>
                    <div class="row">
                        <form method="post" class="form-inline" action="retrieve.php" name="details">
                            <input type="hidden" name="findThis" value="<?= $tag->tag_name; ?>"/>
                            <input type="hidden" name="findID" value="<?= $tag->id; ?>"/>
                            <button class="btn btn-info text-left mx-1" type="submit" name="detail" value="detail">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </form>

                        <form method="post" class="form-inline" action="edit.php" name="details">
                            <input type="hidden" name="findThis" value="<?= $tag->tag_name; ?>"/>
                            <input type="hidden" name="findID" value="<?= $tag->id; ?>"/>
                            <button class="btn btn-warning text-left mx-1" type="submit" name="edit" value="edit">
                                <i class="fa fa-pencil-alt"></i>

                            </button>
                        </form>

                        <form method="post" h class="form-inline" action="delete.php" name="details">
                            <input type="hidden" name="findThis" value="<?= $tag->tag_name; ?>"/>
                            <input type="hidden" name="findID" value="<?= $tag->id; ?>"/>
                            <button class="btn btn-danger mx-1" type="submit" name="delete" value="delete">
                                <i class="fa fa-minus-circle"></i>
                            </button>
                        </form>

                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
<?php

require_once DOC_ROOT . 'templates/footer.php';