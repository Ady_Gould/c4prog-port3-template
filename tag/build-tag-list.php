<?php
/**
 * ONE LINE TITLE FOR FILE
 *
 * LONGER EXPLANATION OF THE FILE AND ITS PURPOSE
 * MAY BE MORE THAN ONE LINE IN LENGTH
 *
 * @File:       /folder-name/build-tag-list.php
 * @Project:    INIT-Port3
 * @Author:     YOUR NAME <Adrian Gould <Adrian.Gould@nmtafe.wa.edu.au>@tafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  YOUR NAME 2018
 *
 * History:
 *  v1.0    03/06/18
 *          Version details
 *
 *  v0.1    2017-05-01
 *          Template as provided by Adrian Gould
 */

require_once __DIR__ . '/../core/init.php';

// get all the tags
$tags = new Tag();
$tags->findAll();
$tagResults = $tags->data();

// get all the acronyms
$acronyms = new Acronym();
$acronyms->findAll();
$acronymResults = $acronyms->results();
$countAcronyms = count($acronymResults);

set_time_limit(0);
ob_implicit_flush(true);
ob_end_flush();

if (is_array($acronymResults)) {
    $i = 0;
    $tagArray = [];
    $tagList = null;
    //$response = array('message' =>  'Starting', 'progress' => '', 'results' => $tagResults, 'acronyms' => $acronymResults);
    foreach ($acronymResults as $k => $r) {
        $i++;
        // sleep(1);
        $p = (int)($i / $countAcronyms * 100); //Progress
        $tagData = $r->tags;
        $tagArray = explode(', ', $tagData);

        //https://developer-paradize.blogspot.com/2013/12/how-to-remove-stop-words-from-string.html
        $tagArray = array_diff($tagArray, $stopWordList);

        foreach ($tagArray as $key => $datum) {
            $oneTag = new Tag();
            if ($datum > '' && !$oneTag->find($datum)) {
                $oneTag->create(
                    [
                        'tag_name' => $datum,
                        'created' => date('Y-m-d H:i:s'),
                        'modified' => date('Y-m-d H:i:s'),
                    ]
                );
            } // end if
        } // end foreach

        $tagList = mergeTags($tagArray);
        $response = array('message' => $p . '% ', 'progress' => $p, 'results' => $tagList);
        echo json_encode($response);

        for ($z = 0; $z < 3000000; $z++) {
            $zx = sin($z);
        }
    } // end foreach


}

function mergeTags($array)
{
    $tagArray = array_unique($array);
    natsort($tagArray);
    $tagString = implode(", ", $tagArray);
    $tagString = str_replace(",,", ", ", $tagString);
    $tagString = str_replace(",", ", ", $tagString);
    $tagString = str_replace(",  ", ", ", $tagString);

    if (substr($tagString, 0, 2) === ', ') {
        $tagString = substr($tagString, 2);
    }
    if (substr($tagString, -1, 1) === ',') {
        $tagString = substr($tagString, -1);
    }
    return $tagString;
}
//
//sleep(1);
//$response = array('message' => 'Complete', 'progress' => 100);
//
//echo json_encode($response);