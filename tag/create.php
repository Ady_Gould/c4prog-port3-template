<?php
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Create | Tag');
$message = new Message();
$user = new User();

// Is user logged in, if not send back to home page...
if (!$user->isLoggedIn()) {
    $message->add("warning", "You must be logged in to add tags...");
    header("Location: index.php");
}


if (Input::exists()) {
    if (Token::check(Input::get('token'))) {

        $message->clear();

        $validate = new Validate();
        $validation = $validate->check($_POST, array(
            'tag_name' => array(
                'required' => true,
                'min' => 2,
                'max' => 128,
                'unique' => 'tags'
            )
        ));

        if ($validation->passed()) {
            $tag = new Tag();

            try {
                $tag->create(array(
                    'tag_name' => Input::get('tag_name'),
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                ));
                $message->add('Success', 'Tag Added.');
            } catch (Exception $e) {
                $message->add("danger", $e->getMessage());
            }
        } else {
            foreach ($validation->errors() as $error => $msg) {
                $message->add($error, $msg);
            }
        }
    }
} else {
    $message->clear();
}

require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Tags: Add New tag</h1>
                <p>This version by: <em>Adrian Gould</em></p></div>
            <div class="col-2">
                <a href="<?= BASE_URL ?>tag/" class="text-light nav-link"><i
                            class="fas fa-tag fa-7x mx-1 text-light"></i></a>
            </div>
        </div>
    </div>
<?php

$messages = $message->messages();
if ($messages > '' && $messages->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-<?= ($error == 'Success' ? 'success' : 'danger'); ?> alert-dismissible">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i><strong>  <?= $error; ?></strong></span>
            <span class="col-9"><?= $message; ?></span>
        </p>
        <?php
    } // end foreach
} // end if messages
?>
    <form action="create.php" method="post">
        <div class="form-group">
            <label for="tag_name">New Tag</label>
            <input class="form-control" type="text" name="tag_name" id="tag_name"
                   value="<?php echo escape(Input::get('tag_name')); ?>"
                   autocomplete="off"/>
        </div>

        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>"/>
        <button class="btn btn-primary" type="submit" value="addTag">Add Tag</button>

    </form>

<?php

require_once DOC_ROOT . 'templates/footer.php';