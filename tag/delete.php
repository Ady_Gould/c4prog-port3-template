<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Delete | Tag');
$messages = new Message();

$user = new User();
// if user is not an admin or moderator then refuse delete, return to index

if (!$user->isLoggedIn() || !$user->hasPermission(['moderator', 'admin'])) {
    $messages->clear();
    $messages->add('Warning', 'Sorry but only administrative staff are allowed to delete tags.');
    Redirect::to(BASE_URL . 'tag/index.php');
}


$tags = new Tag();

if (Input::exists() && Input::get('findThis')) {
    $findMe = trim($_POST['findThis']);
    $findID = trim($_POST['findID']);
    //$tags->delete(['tag_name','=',$findMe]);
    // header("redirect: delete.php");
} elseif (Input::get('tag') && Input::get('deleteTag')) {
    $result = $tags->delete('id', Input::get('tag'));
    $messages->add('Success', 'Tag, ' . Input::get('deleteThis') . ' deleted from list');
    header('Location: index.php');

} else {

    header('Location: index.php');
}

$tags->find((int)$findID);
$numFound = $tags->count();


// make sure the data returned is in an array for processing in the page
$data = [];
if ($tags->count() > 0) {
    if (is_array($tags->data())) {
        $data = $tags->data();
    } else {
        $data = [$tags->data()];
    }
}

require_once DOC_ROOT . 'templates/header.php';

?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Delete Tags</h1>
                <p>This version by: <em>Adrian Gould</em></p></div>
            <div class="col-2">
                <i class="fas fa-tag fa-7x mx-1 text-light"></i>
            </div>
        </div>
    </div>
<?php

?>
    <div class="row">
        <form action="delete.php" method="post" class="col-12">
            <h2 class="">Are you sure you wish to delete the tag <strong class="text-primary"><?=
                    $data[0]->tag_name; ?></strong>?</h2>
            <input class="form-control mr-sm-2" type="hidden" id="tag" name="tag" value="<?= $data[0]->id; ?>">
            <input class="form-control mr-sm-2" type="hidden" id="deleteThis" name="deleteThis"
                   value="<?= $data[0]->tag_name; ?>">

            <button class="btn btn-danger m-2" name="deleteTag" value="Delete">
                <i class="fa fa-minus-circle mx-1"></i>
                Yes, Delete
            </button>
            <button class="btn btn-primary m-2" name="cancelDelete" value="Cancel">
                <i class="fa fa-times-circle mx-1"></i>
                No, Cancel
            </button>
        </form>
    </div>
<?php

require_once DOC_ROOT . 'templates/footer.php';