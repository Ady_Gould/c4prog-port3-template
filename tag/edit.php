<?php
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Edit | Tag');
$message = new Message();
$tag = new Tag();

// check if user is logged in and a
$user = new User();
if (!$user->isLoggedIn() || !$user->hasPermission(['moderator', 'admin'])) {
    $message->clear();
    if (!$user->isLoggedIn()) {
        $message->add('Warning', 'Sorry but only you must be logged in to edit tags.');
    }
    $message->add('Warning', 'Sorry but only administrative staff are allowed to edit tags.');
    Redirect::to(BASE_URL . 'tag/index.php');
}

if (Input::exists() && Input::get('findThis') && Input::get('findID')) {
    $findMe = trim(Input::get('findThis'));
    $findID = trim(Input::get('findID'));
    $tag->find((int)$findID);
} else {
    if (Input::exists() && Input::get('tagID') && Input::get('tag_name') && Input::get('saveTag')) {
        if (Token::check(Input::get('token'))) {

            $message->clear();
            $validate = new Validate();
            $validation = $validate->check($_POST, array(
                'tag_name' => array(
                    'required' => true,
                    'min' => 2,
                    'max' => 128,
                    'unique' => 'tags',
                ),
            ));

            if ($validation->passed()) {

                try {
                    $tag->update(
                        array(
                            'tag_name' => Input::get('tag_name'),
                            'modified' => date('Y-m-d H:i:s'),
                        ), Input::get('tagID'));
                    $message->add('Success', 'Tag Edit Saved');
                } catch (Exception $e) {
                    $message->add("Danger", $e->getMessage());
                }
                header('Location: index.php');
            } else {
                foreach ($validation->errors() as $error => $msg) {
                    $message->add($error, $msg);
                }
            }
        } else {
            $message->clear();
        }
    }
}

require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Tags: Edit tag</h1>
                <p>This version by: <em>Adrian Gould</em></p></div>
            <div class="col-2">
                <a href="<?= BASE_URL ?>tag/" class="text-light nav-link">
                    <i class="fas fa-tag fa-7x mx-1 text-light"></i>
                </a>
            </div>
        </div>
    </div>
<?php

$messages = $message->messages();
if ($messages > '' && $message->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-<?= $message->errorColour($error); ?>
        alert-dismissible">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i>
                <strong><?= $error; ?></strong>
            </span>
            <span class="col-9"><?= $message; ?></span>
        </p>
        <?php
    } // end foreach
} // end if messages
?>
    <form action="edit.php" method="post">
        <div class="form-group">
            <label for="tag_name">Tag to Edit</label>
            <input class="form-control" type="text" name="tag_name" id="tag_name"
                   value="<?= escape(Input::get('tag_name') ? Input::get('tag_name') : $findMe); ?>"
                   autocomplete="off"/>
        </div>

        <input type="hidden" id="token" name="token" value="<?= Token::generate(); ?>"/>
        <input type="hidden" id="tagID" name="tagID"
               value="<?= Input::get('tagID') ? Input::get('tagID') : $findID; ?>"/>
        <button class="btn btn-primary" id="saveTag" name="saveTag" type="submit" value="saveTag">Save Tag</button>

    </form>

<?php

require_once DOC_ROOT . 'templates/footer.php';