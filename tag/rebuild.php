<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Rebuild | Tag');
$messages = new Message();

$user = new User();
// if user not logged in and not an admin then redirect to home index page
if (!$user->isLoggedIn() || !$user->hasPermission(['admin', 'moderator'])) {
    header("Location: " . BASE_URL);
}

require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Tags</h1>
                <p class="lead">Rebuild from Acronym List</p>
                <p class="small">This version by: <em>Adrian Gould</em></p>
            </div>
            <div class="col-2">
                <a href="<?= BASE_URL ?>tag/" class="text-light nav-link">
                    <i class="fas fa-tag fa-7x mx-1 text-light"></i>
                </a>
            </div>
        </div>
    </div>
<?php

?>
    <div class="row">
        <h2 class="col">Rebuilding Tag List...</h2>
    </div>
    <div class="row">
        <div class="col">
            <div class="progress" style="40px">
                <div id="divProgress"
                     class="progress-bar progress-bar-animated progress-bar-striped text-left p-1 fa-2x"
                     role="progressbar" style="width: 0;"
                     aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col" id="BOO">
            <!-- message area whilst processing -->
        </div>
    </div>

<?php
require_once DOC_ROOT . 'templates/footer.php';
?>
    <script src="<?= BASE_URL; ?>assets/js/rebuild-tag-list.js"></script>

    <script>ajax_stream('<?=BASE_URL;?>/tag/build-tag-list.php')</script>
<?php
