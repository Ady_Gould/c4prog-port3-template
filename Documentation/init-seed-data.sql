/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

-- ACRONYM SEED DATA

INSERT INTO `acronyms` (`id`, `acronym`, `definition`, `acronym_type`, `user_id`, `tags`, `created`, `modified`) VALUES
  (1, 'KISS', 'Keep it Simple, Stupid', 'acronym', 1, 'acronym, design, programming', '2018-05-01 13:24:00',
   '2018-05-01 13:24:00'),
  (2, 'CRUD', 'Create, Retrieve, Update, Delete', 'Acronym', 3, 'computing, database', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (3, 'IDE', 'Integrated Development Environment', 'Initialism', 2, 'computing, programming', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (4, 'PHP', 'PHP Hypertext Processor', 'Initialism', 1, 'computing,', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (5, 'ROY G BIV', 'Red Orange Yellow Green Blue Indigo Violet', 'Mnemonic', 4, 'design, colour', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (6, 'JS', 'JavaScript', 'Initialism', 2, 'computing, web, programming, mobile', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (7, 'WAN', 'Wide Area Network', 'Acronym', 2, 'computing, network', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (8, 'HTML', 'HyperText Markup Language', 'Initialism', 3, 'computing', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (9, 'CSS', 'Cascading Style Sheets', 'Initialism', 2, 'web, computing, design, layout', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (10, 'ABC', 'Australian Broadcasting Corporation', 'Initialism', 4, 'broadcasting, television, tv, company', '2018-05-01 14:19:37', '2018-05-01 14:19:37'),
  (11, 'ARIA', 'Accessible Rich Internet Applications', 'acronym', 3, 'acronym, web, accessibility', '2018-04-30 09:37:48', '2018-04-30 09:37:48'),
  (12, 'API', 'Application Programming Interface', 'initialism', 10, 'initialism, software, programming', '2018-04-30 09:37:51', '2018-04-30 09:37:51'),
  (13, 'ECMAScript', 'European Computer Manufacturers Association Script<br>Pronounced Ek-Ma-Script', 'acronym', 6, 'acronym, web, programming, software', '2018-04-30 09:38:07', '2018-04-30 09:38:07'),
  (14, 'ES', 'ECMAScript', 'initialism', 1, 'acronym, web, programming, software, language', '2018-04-30 09:38:58', '2018-04-30 09:38:58'),
  (15, 'IDE', 'Integrated Development Environment', 'initialism', 7, 'initialism, software, programming, language', '2018-04-30 09:39:11', '2018-04-30 09:39:11'),
  (16, 'VRML', 'Virtual Reality Modelling Language', 'initialism', 3, 'initialism, software, programming', '2018-04-30 09:40:16', '2018-04-30 09:40:16'),
  (17, 'WAIS', 'Wide Area Information Server', 'acronym', 3, 'acronym, networking', '2018-04-30 09:40:31', '2018-04-30 09:40:31'),
  (18, 'WAN', 'Wide Area Network', 'acronym', 5, 'acronym, networking', '2018-04-30 09:40:34', '2018-04-30 09:40:34'),
  (19, 'WEP', 'Wired Equivalent Privacy', 'acronym', 10, 'acronym, networking', '2018-04-30 09:41:00', '2018-04-30 09:41:00'),
  (20, 'WI-FI', 'Wireless Fidelity', 'acronym', 7, 'acronym, networking', '2018-04-30 09:42:13', '2018-04-30 09:42:13'),
  (21, 'WPA', 'Wi-Fi Protected Access', 'initialism', 8, 'acronym, networking', '2018-04-30 09:42:43', '2018-04-30 09:42:43'),
  (22, 'WWW', 'World Wide Web', 'initialism', 3, 'initialism, web', '2018-04-30 09:43:20', '2018-04-30 09:43:20'),
  (23, 'XHTML', 'Extensible Hypertext Mark—up Language', 'initialism', 2, 'initialism, web', '2018-04-30 09:44:33', '2018-04-30 09:44:33'),
  (24, 'XML', 'Extensible Mark—up Language', 'initialism', 6, 'initialism, web', '2018-04-30 09:45:43', '2018-04-30 09:45:43'),
  (25, 'XSLT', 'Extensible Style Sheet Language Transformation', 'initialism', 7, 'initialism, web', '2018-04-30 09:47:03', '2018-04-30 09:47:03'),
  (26, 'Y2K', 'Year 2000', 'acronym', 6, 'acronym, data, dates', '2018-04-30 09:47:26', '2018-04-30 09:47:26'),
  (27, 'TL;DR', 'Too long; didn\'t read', 'initialism', 6, 'initialism, software, programming, blog, copy', '2018-04-30 09:48:18', '2018-04-30 09:48:18'),
  (28, 'IMHO', 'In my humble opinion', 'initialism', 6, 'initialism, software, programming, chat', '2018-04-30 09:48:23', '2018-04-30 09:48:23'),
  (29, 'LGTM', 'Looks good to me', 'initialism', 5, 'initialism, software, programming, blog, copy', '2018-04-30 09:49:06', '2018-04-30 09:49:06'),
  (30, 'ACID', 'Atomicity, consistency, isolation, durability', 'acronym', 8, 'acronym, fun, database', '2018-04-30 09:49:43', '2018-04-30 09:49:43'),
  (31, 'DAP', 'Domain Application Protocol', 'acronym', 3, 'acronym, networking', '2018-04-30 09:50:50', '2018-04-30 09:50:50'),
  (32, 'SOA', 'Service-oriented architecture', 'initialism', 7, 'initialism, software, programming, web', '2018-04-30 09:51:16', '2018-04-30 09:51:16'),
  (33, 'SOAP', 'Simple Object Access Protocol [originally]', 'acronym', 5, 'acronym, programming, software', '2018-04-30 09:52:30', '2018-04-30 09:52:30'),
  (34, 'BREAD', 'Browse, Read, Edit, Add, Delete', 'acronym', 2, 'acronym, programming, software, database', '2018-04-30 09:53:01', '2018-04-30 09:53:01'),
  (35, 'DAVE', 'Delete, Add, View, Edit', 'acronym', 2, 'acronym, programming, software, database', '2018-04-30 09:53:33', '2018-04-30 09:53:33'),
  (36, 'CRAP', 'Create, Replicate, Append, Process', 'acronym', 2, 'acronym, programming, software', '2018-04-30 09:54:56', '2018-04-30 09:54:56'),
  (37, 'JSON', 'JavaScript Object Notation', 'acronym', 6, 'acronym, programming, software', '2018-04-30 09:56:18', '2018-04-30 09:56:18'),
  (38, 'LAMP', 'Linux, Apache, MySQL, PHP/Python/Perl', 'acronym', 3, 'acronym, programming, software, web', '2018-04-30 09:56:51', '2018-04-30 09:56:51'),
  (39, 'MEAN', 'MongoDB, Express.js, Angular, Node.js', 'acronym', 8, 'acronym, programming, software, web', '2018-04-30 09:57:11', '2018-04-30 09:57:11'),
  (40, 'MVC', 'Model-View-Controller', 'initialism', 3, 'acronym, programming, software', '2018-04-30 09:58:29', '2018-04-30 09:58:29'),
  (41, 'REGEX', 'Regular Expression', 'acronym', 8, 'acronym, programming, software', '2018-04-30 09:59:37', '2018-04-30 09:59:37'),
  (42, 'REST', 'Representational State Transfer', 'acronym', 7, 'acronym, programming, software, web', '2018-04-30 10:01:00', '2018-04-30 10:01:00'),
  (43, 'BASIC', 'Beginner\'s All-purpose Symbolic Instruction Code', 'acronym', 10, 'acronym, programming, software, language', '2018-04-30 10:02:12', '2018-04-30 10:02:12'),
  (44, 'CGI', 'Common Gateway Interface', 'initialism', 7, 'initialism, software, programming', '2018-04-30 10:03:27', '2018-04-30 10:03:27'),
  (45, 'CISC', 'Complex Instruction Set Computing<br>pronounced sisk', 'acronym', 3, 'acronym, hardware, processors', '2018-04-30 10:04:45', '2018-04-30 10:04:45'),
  (46, 'CMOS', 'Complementary Metal Oxide Semiconductor<br>pronouned See-Moss', 'acronym', 7, 'acronym, hardware', '2018-04-30 10:05:49', '2018-04-30 10:05:49'),
  (47, 'DBMS', 'Database Management System', 'initialism', 2, 'initialism, software, programming, database', '2018-04-30 10:07:02', '2018-04-30 10:07:02'),
  (48, 'ACL', 'Access Control List', 'initialism', 7, 'initialism, software, programming, security', '2018-04-30 10:07:47', '2018-04-30 10:07:47'),
  (49, 'FAQ', 'Frequently Asked Questions<br>Pronounced Fack, or Ef-Ay-Queue', 'acronym', 7, 'acronym, initialism, design, software', '2018-04-30 10:08:06', '2018-04-30 10:08:06'),
  (50, 'FIFO', 'First In, First Out', 'acronym', 9, 'acronym, networking, software, programming, queue', '2018-04-30 10:09:19', '2018-04-30 10:09:19'),
  (51, 'HTML', 'Hyper-Text Markup Language', 'initialism', 9, 'initialism, software, programming, web', '2018-04-30 10:10:29', '2018-04-30 10:10:29'),
  (52, 'HTTP', 'HyperText Transfer Protocol', 'initialism', 2, 'initialism, web, protocol, networking',
   '2018-04-30 10:10:44', '2018-04-30 10:10:44'),
  (53, 'HTTPS', 'HyperText Transport Protocol Secure', 'initialism', 8, 'initialism, web, protocol, networking',
   '2018-04-30 10:11:02', '2018-04-30 10:11:02'),
  (54, 'All People Seem To Need Data Processing',
   'Application, Presentation, Session, Transport, Network, Data Link, Physical', 'mnemonic', 10,
   'mnemonic, networking', '2018-04-30 10:12:22', '2018-04-30 10:12:22'),
  (55, 'ASCII', 'American Standard Code for Information Interchange<br>Pronounced: As-Key', 'acronym', 2,
   'acronym, computing, data, software, programming', now(), now()),
  (56, 'BOM', 'Bureau of Meteorology', 'acronym', 1, 'acronym, government', now(), now()),
  (57, 'My Very Excited Mother Just Served Us Nine Pies',
   'Planetary order: Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune, and Pluto', 'mnemonic', 3,
   'planets, mnemonic', now(), now()),
  (58, 'Dec.', 'December', 'abbreviation', 1, 'abbreviation, month, calendar', now(), now()),
  (59, 'DRY', 'Don\'t Repeat Yourself', 'acronym', 2, 'acronym, programming, software, design', now(), now()),
  (60, 'RICE', 'Rest the injured area, Ice the sprain, Compress with a wrap or bandage, Elevate the injured area',
   'mnemonic', 3, 'mnemonic, acronym, injury, medical', now(), now());

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

-- USERS STRUCTURE AND SEED DATA

CREATE TABLE `users` (
  `id`            int(10) UNSIGNED NOT NULL,
  `user_name`     varchar(24)      NOT NULL,
  `user_password` varchar(64)      NOT NULL,
  `given_name`    varchar(64)      NOT NULL,
  `last_name`     varchar(64)      NOT NULL,
  `user_picture`  varchar(190)     NOT NULL DEFAULT 'blank.png',
  `salt`          blob             NOT NULL,
  `user_group`    varchar(32)      NOT NULL DEFAULT 'user',
  `created`       datetime         NOT NULL DEFAULT '0001-01-01 00:00:00',
  `modified`      datetime         NOT NULL DEFAULT '0001-01-01 00:00:00'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

INSERT INTO `users` (`id`, `user_name`, `user_password`, `given_name`, `last_name`, `user_picture`, `salt`, `user_group`, `created`, `modified`)
VALUES
  (1, 'admin', '23ed5b53a951c376148e93509c2069b440e47fb304d2f8c819386f776adef99e', 'Ad', 'Minion II', 'ad-minion.png',
   0x05dacf56eb5e677a29ea40df8926d901087deddc2401588a24e14b17f95caa7b, 'admin', '2018-05-28 09:18:12',
   '2018-05-28 09:18:12'),
  (2, 'jack', '6405c191d12150fa67924b820444d894d0c3cdceb753251a274528a95d4faa92', 'Jacques', 'd\'Carre',
   'jacques-dcarre.png', 0x15489239ec092be42d816d4bab352a06cc84b122a002d7f191be538f37387fbb, 'user',
   '2018-05-28 09:18:35', '2018-05-28 09:18:35'),
  (3, 'adrian', '33234a288ca702b620c787b8128073ae8987e3e14d7c0c547611cf106e9f51a1', 'Ady', 'Gould', 'ady-gould.png',
   0x3ea1b44050ec1d42b4c0e0db252a7132cd38c0cb85b07e70107bcb8311d98f8d, 'moderator', '2018-05-28 09:18:55',
   '2018-05-28 09:18:55'),
  (4, 'robin', '816b09469a78cce84e4394387f1d140b3b78d3323b6512c8af53f49c1e9bb19d', 'Robin', 'Banks', 'robin-banks.png',
   0xc691d1c782e9d15a88c8422c38204556eec3a47d2280dffdc754bc6b2cc278a1, '1', now(), now()),
  (5, 'april', '2763d0546590df9de40acb86410973e8b4468dd48e454168ff516487a44038b0', 'April', 'May', 'april-may.png',
   0xa48be879645a8a0957b15e8e109b7eaf69ba6f2b1b6224736f078ec676467263, '1', now(), now()),
  (6, 'dino', '1af70c30f969b00683e4c6951ee54e2b740fdfa4f94fa85c534dfdd4f32b0ca7', 'Dinah', 'Sore', 'dinah-sore.png',
   0xd15e1fb51bc6796c56b3d502dadf3d59290ee4dd0f81ead1941a8d2bfe1f2135, '1', now(), now()),
  (7, 'ferris', '4f29c8ee153e406bcf9b2a02941e612a238f24eab7d1549b2caca8caeddcbe81', 'Ferris', 'Wheel',
   'ferris-wheel.png', 0xf7791957321afe50825b57b35864cfacd88d1d3b19fba5f9ebe88537f3bec362, '1', now(), now());


ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 8;
COMMIT;

-- GROUP STRUCTURE AND SEED DATA

CREATE TABLE `groups` (
  `id`                int(11)     NOT NULL,
  `group_name`        varchar(32) NOT NULL,
  `group_description` varchar(32) NOT NULL,
  `group_permissions` text        NOT NULL,
  `created`           datetime    NOT NULL DEFAULT '0001-01-01 00:00:00',
  `modified`          datetime    NOT NULL DEFAULT '0001-01-01 00:00:00'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

INSERT INTO `groups` (`id`, `group_name`, `group_description`, `group_permissions`, `created`, `modified`) VALUES
  (3, 'user', 'Standard User', '{"user":1}', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
  (1, 'admin', 'Administrator', '{"admin":1}', '0001-01-01 00:00:00', '0001-01-01 00:00:00'),
  (2, 'moderator', 'Moderator', '{"moderator":1}', '0001-01-01 00:00:00', '0001-01-01 00:00:00');

ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
  AUTO_INCREMENT = 4;
COMMIT;

CREATE TABLE `user_sessions` (
  `id`      int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `hash`    varchar(190)     NOT NULL,
  `created` datetime         NOT NULL DEFAULT '0001-01-01 00:00:00',
  `updated` datetime         NOT NULL DEFAULT '0001-01-01 00:00:00'
)
  ENGINE = InnoDB
  DEFAULT CHARSET = latin1;

ALTER TABLE `user_sessions`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `user_sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

insert into tags (`tag_name`, `created`, `modified`) values
  ('accessibility', now(), now()),
  ('acronym', now(), now()),
  ('language', now(), now()),
  ('broadcasting', now(), now()),
  ('colour', now(), now()),
  ('company', now(), now()),
  ('computing', now(), now()),
  ('database', now(), now()),
  ('design', now(), now()),
  ('initialism', now(), now()),
  ('language', now(), now()),
  ('layout', now(), now()),
  ('mobile', now(), now()),
  ('network', now(), now()),
  ('networking', now(), now()),
  ('programming', now(), now()),
  ('software', now(), now()),
  ('television', now(), now()),
  ('web', now(), now());


/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
