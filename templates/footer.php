<?php
/**
 * Footer file for Episode 13
 *
 * @File:       /Ep13/footer.php
 * @Project:    Demo-PHP
 * @Author:     Adrian Gould <adrian.gould@nmtafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Adrian Gould, 2018
 *
 * History:
 *  v1.0    08/05/18
 *          Initial Home Page File for Episode 12
 *
 *  v0.1    2017-05-01
 *          Template as provided by Adrian Gould
 */
?>
</div><!-- end container -->


<footer class="footer bg-dark text-light">
    <div class="container">

        <div class="row">

            <div class="col-md-6 col-xs-12 small">
                <ul class="list-unstyled">
                    <li class="list-inline-item">&copy; Copyright 2018 <strong>YOUR NAME</strong></li>
                </ul>
            </div>

            <div class="col-md-6 col-xs-12 small">
                <ul class="list-unstyled">
                    <li class="list-inline-item">
                        <a href="http://twitter.com">
                            <i class="fab fa-twitter"></i>
                            Twitter
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://facebook.com">
                            <i class="fab fa-facebook"></i>
                            Facebook
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="http://linkedin.com">
                            <i class="fab fa-linkedin"></i>
                            Linked in
                        </a>
                    </li>
                </ul>
            </div>

        </div>

        <div class="row">

            <div class="col-md-6 col-xs-12 small">
                <ul class="list-unstyled">
                    <li class="list-inline-item"><a href="<?= BASE_URL; ?>">Contact Us</a></li>
                    <li class="list-inline-item"><a href="<?= BASE_URL; ?>">Site map</a></li>
                    <li class="list-inline-item"><a href="<?= BASE_URL; ?>">Privacy policy</a></li>
                    <li class="list-inline-item"><a href="<?= BASE_URL; ?>">Terms and Conditions</a></li>
                </ul>
            </div>

            <div class="col-md-6 col-xs-12 small">
                <ul class="nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button"
                           aria-haspopup="true" aria-expanded="false">
                            <i class="fab fa-youtube-square"></i> Associated Tutorials
                        </a>
                        <div class="dropdown-menu bg-light">
                            <a class="dropdown-item"
                               href="https://www.youtube.com/playlist?list=PLfdtiltiRHWHjTPiFDRdTOPtSyYfz3iLW">
                                <i class="fab fa-youtube-square"></i> PHP Basics
                            </a>
                            <a class="dropdown-item"
                               href="https://www.youtube.com/playlist?list=PLfdtiltiRHWF0RicJb20da8nECQ1jFvla">
                                <i class="fab fa-youtube-square"></i> PHP OOP
                            </a>
                            <a class="dropdown-item"
                               href="https://www.youtube.com/playlist?list=PLfdtiltiRHWF5Rhuk7k4UAU1_yLAZzhWc">
                                <i class="fab fa-youtube-square"></i> OOP/Login System
                            </a>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
</footer>


<script src="<?= BASE_URL; ?>assets/js/jquery-3.3.1.min.js"></script>
<script src="<?= BASE_URL; ?>assets/js/bootstrap.bundle.min.js"></script>
<script src="<?= BASE_URL; ?>assets/js/fontawesome-all.js"></script>

<!-- include page specific JS files -->

<!-- end of page specific JS files -->
</body>
</html>
