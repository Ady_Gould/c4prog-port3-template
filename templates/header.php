<?php
/**
 * Header file for Portfolio 3
 *
 * @File:       /template/header.php
 * @Project:    Demo-PHP
 * @Author:     Adrian Gould <adrian.gould@nmtafe.wa.edu.au>
 * @Version:    1.0
 * @Copyright:  Adrian Gould, 2018
 *
 * History:
 *  v1.0    08/05/18
 *          Initial Home Page File for Episode 12
 *
 *  v0.1    2017-05-01
 *          Template as provided by Adrian Gould
 */
$user = new User();
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= Session::get('title'); ?> | <?= Config::get('sitename'); ?></title>

    <!-- link in bootstrap -->
    <link rel="stylesheet" href="<?= BASE_URL; ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= BASE_URL; ?>/assets/css/app.css">
    <!--core first + styles last-->
    <link href="<?= BASE_URL; ?>/assets/css/fontawesome-all.css" rel="stylesheet">

</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">

    <a href="<?= BASE_URL; ?>" class="navbar-brand">Portfolio 3</a>

    <button class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#mainNav"
            aria-controls="#mainNav"
            aria-expanded="false" aria-label="toggle">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="mainNav">
        <ul class="navbar-nav mr-auto">

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="<?= BASE_URL; ?>acronym/" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-book mx-1 text-warning"></i> Acronyms
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <h6 class="dropdown-header">Details, Search, Edit, Delete all from Home</h6>
                    <div class="dropdown-divider"></div>
                    <a href="<?= BASE_URL; ?>acronym/index.php" class="dropdown-item text-muted">Home</a>
                    <a href="<?= BASE_URL; ?>acronym/create.php" class="dropdown-item text-dark">Create</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="<?= BASE_URL; ?>tag/" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-tag mx-1 text-success"></i> Tags
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <h6 class="dropdown-header">Details, Search, Edit, Delete all from Home</h6>
                    <div class="dropdown-divider"></div>
                    <a href="<?= BASE_URL; ?>tag/index.php" class="dropdown-item text-muted">Home</a>
                    <a href="<?= BASE_URL; ?>tag/create.php" class="dropdown-item text-dark">Create</a>
                    <div class="dropdown-divider"></div>
                    <a href="<?= BASE_URL; ?>tag/rebuild.php" class="dropdown-item text-dark">Rebuild</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="<?= BASE_URL; ?>user/" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user mx-1 text-primary"></i> Users
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a href="<?= BASE_URL; ?>user/index.php" class="dropdown-item text-muted">Home</a>
                    <a href="<?= BASE_URL; ?>user/create.php" class="dropdown-item text-dark">Create</a>
                    <a href="<?= BASE_URL; ?>user/retrieve.php" class="dropdown-item text-dark">Retrieve</a>
                    <a href="<?= BASE_URL; ?>user/search.php" class="dropdown-item text-dark">Search</a>
                    <a href="<?= BASE_URL; ?>user/update.php" class="dropdown-item text-dark">Update</a>
                    <a href="<?= BASE_URL; ?>user/delete.php" class="dropdown-item text-dark">Delete</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="<?= BASE_URL; ?>tag/" id="navbarDropdown" role="button"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-question mx-1 text-info"></i> QUERIES
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <h6 class="dropdown-header">Answers to the queries</h6>
                    <div class="dropdown-divider"></div>
                    <a href="<?= BASE_URL; ?>query/query-01.php" class="dropdown-item text-muted">Query 01</a>
                    <a href="<?= BASE_URL; ?>query/query-02.php" class="dropdown-item text-muted">Query 02</a>
                    <a href="<?= BASE_URL; ?>query/query-03.php" class="dropdown-item text-muted">Query 03</a>
                </div>
            </li>
        </ul>

        <ul class="navbar-nav mt-2 mt-md-0 mr-5">
            <?php
            if ($user->isLoggedIn()) {
                ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="<?= BASE_URL; ?>user/" id="navbarUser" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-alt mx-1 text-light"></i>
                        <?= escape($user->data()->given_name) ?>
                        <?= escape($user->data()->last_name) ?>
                    </a>
                    <div class="dropdown-menu bg-dark" aria-labelledby="navbarUser">
                        <a href="<?= BASE_URL; ?>user/profile.php?user=<?= escape($user->data()->user_name); ?>"
                           class="dropdown-item text-light">
                            Profile
                        </a>
                        <a href="<?= BASE_URL; ?>user/update.php" class="dropdown-item text-light">
                            Update Details
                        </a>
                        <a href="<?= BASE_URL; ?>user/changepassword.php" class="dropdown-item text-light">
                            Change Password
                        </a>
                        <a href="<?= BASE_URL; ?>user/logout.php" class="dropdown-item text-light">
                            Logout
                        </a>
                    </div>
                </li>
                <?php
            } else {
                ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="<?= BASE_URL; ?>user/" id="navbarUser" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-alt mx-1 text-light"></i>
                        Login/Register
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarUser">
                        <a href="<?= BASE_URL; ?>user/login.php" class="dropdown-item text-dark">Log In</a>
                        <a href="<?= BASE_URL; ?>user/register.php" class="dropdown-item text-dark">Register</a>
                    </div>
                </li>
                <?php
            }
            ?>

        </ul>
    </div>
</nav>


<div class="container">
