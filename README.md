INIT-Port3
==========

This is base code for Portfolio Part 3.

Not all code is complete, and needs to fixed, or added to solve the problem:

* complete the Acronym class
* complete the acronym index.php page - includes search feature
* complete the acronym create.php page - adds a new acronym (user must be logged in)
* complete the acronym edit.php page - edits the selected acronym (user must be logged in, only edit their own)
* complete the acronym delete.php page - deletes the selected acronym (user must be logged in, only delete their own)
* complete the acronym retrieve.php page - displays the details for an acronym

Moderators and Admin may edit and delete ANY acronym.

