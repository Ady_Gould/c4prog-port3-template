<?php
// Include Core Initialization File
require_once 'core/init.php';

Session::put('title', 'Home');

require_once 'templates/header.php';

if (Session::exists('home')) {
    echo '<p>' . Session::flash('home') . '</p>';
}

$user = new User();

?>
    <div class="jumbotron">
        <div class="row">
            <div class="col-10">
                <h1>Welcome to Portfolio 3
                    <?= $user->isLoggedIn() ? ', ' . escape($user->data()->given_name) : ''; ?> </h1>
                <p>This version by: <em>YOUR NAME HERE</em></p></div>
            <div class="col-2">
                <?php
                if ($user->isLoggedIn()) {
                    if ($user->data()->user_picture !== 'blank.png') {
                        ?>
                        <img src="<?= BASE_URL . 'assets/images/' . $user->data()->user_picture; ?>"
                             alt="Picture of <?= $user->data()->given_name; ?>" class="img-thumbnail">
                        <?php
                    } else {
                        if (!$user->isLoggedIn()) {
                            echo '<p><i class="fas fa-user fa-5x text-secondary"></i></p>';
                        } else {
                            if ($user->hasPermission(['admin'])) {
                                echo '<p><i class="fas fa-user-secret fa-5x text-danger"></i></p>';
                            } elseif ($user->hasPermission(['moderator'])) {
                                echo '<p><i class="fas fa-user fa-5x text-success"></i></p>';
                            } else {
                                echo '<p><i class="fas fa-user fa-5x text-secondary"></i></p>';
                            } // end if
                        }
                    }
                } // end logged in
                ?>
            </div>
        </div>

    </div>
<?php
if (!$user->isLoggedIn()) {

    echo '<p>You need to <a href="user/login.php">login</a> or <a href="user/register.php">register</a></p>';
}


require_once 'templates/footer.php';