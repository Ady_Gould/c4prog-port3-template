<?php
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Create | Acronym');
$messages = new Message();

// check if user is logged in and a
$user = new User();
if (!$user->isLoggedIn()) {
    $messages->clear();
    if (!$user->isLoggedIn()) {
        $messages->add('Information', 'Sorry but only you must be logged in to edit acronyms.');
        Redirect::to(BASE_URL . 'acronym/index.php');
    }
    //$messages->add('Warning','Sorry but only administrative staff are allowed to edit acronyms.');
}

if (Input::exists() && Input::get('findThis')) {
    $findMe = trim($_POST['findThis']);
    $findID = trim($_POST['findID']);
} elseif (Input::get('acronymID') && Input::get('acronym_name') && Input::get('saveAcronym')) {
    if (Input::exists()) {
        if (Token::check(Input::get('token'))) {
            $messages->clear();
            $validate = new Validate();
            $validation = $validate->check($_POST,
                [
                    'acronym_name' => [
                        'required' => true,
                        'min' => 2,
                        'max' => 128,
                        'unique' => 'acronyms',
                    ],
                    [
                        // add validation for the acronym description
                    ],
                    [
                        // add validation for the acronym type
                    ],
                    [
                        // add validation for the acronym tags
                    ],
                ]
            );

            if ($validation->passed()) {
                $acronym = new Acronym();

                try {
                    $acronym->update(
                        [
                            // TODO: Correct this code so it puts the correct form field data into the array
                            // TODO: NB: this uses associative arrays, see tag create for an example
                            acronym => Input::get(ACRONYM_NAME),
                            acronym_type => Input::get(ACRONYM_TYPE),
                            definition => Input::get(DEFINITION),
                            tags => Input::get(ACRONYM_TAGS),
                            user_id => $userID,
                            'modified' => date('Y-m-d H:i:s'),
                        ]
                    );
                    $messages->add('Success', 'Acronym Edit Saved');
                } catch (Exception $e) {
                    $messages->add("Danger", $e->getMessage());
                }
            } else {
                foreach ($validation->errors() as $error => $msg) {
                    $messages->add($error, $msg);
                }
            }
        }
    } else {
        $messages->clear();
    }

} else {

    header('Location: index.php');
}

require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Acronyms: Edit acronym</h1>
                <p>This version by: <em>Adrian Gould</em></p></div>
            <div class="col-2">
                <a href="<?= BASE_URL ?>acronym/" class="text-light nav-link">
                    <i class="fas fa-book fa-7x mx-1 text-light"></i>
                </a>
            </div>
        </div>
    </div>
<?php

if ($messages > '' && $messages->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-<?= $messages->errorColour($error); ?>
        alert-dismissible">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i>
                <strong><?= $error; ?></strong>
            </span>
            <span class="col-9"><?= $message; ?></span>
        </p>
        <?php
    } // end foreach
} // end if messages
?>
    <!--
    TODO: Correct the code in the form so that the correct data is displayed in the relevant areas
    TODO: Important: use of TODO: or CAPITALS FOR PHRASES generally indicate that change(s) may be needed.
    -->
    <form action="SELECT CORRECT DESTINATION SCRIPT" method="SELECT CORRECT METHOD">
        <div class="form-group">
            <label for="acronymName">Acronym</label>
            <input class="form-control" type="text" name="acronym_name" id="acronymName"
                   value="<?php //TODO: GET THE ACRONYM NAME AND DISPLAY HERE ?>"
                   autocomplete="off"/>
        </div>

        <div class="form-group">
            <label for="acronymType">Type</label>
            <select class="custom-select" id="acronymType" name="acronym_type">
                <option selected disabled>Choose...</option>
                <option value="abbreviation">Abbreviation</option>
                <option value="acronym">Acronym</option>
                <option value="initialism">Initialism</option>
                <option value="mnemonic">Mnemonic</option>
                <option value="other">Other</option>
            </select>
        </div>

        <div class="form-group">
            <label for="acronymDefinition">Definition</label>
            <textarea class="form-control" type="textarea" name="acronym_definition" id="acronymDefinition"
                      autocomplete="off"><?php //TODO: GET THE ACRONYM DEFINITION AND DISPLAY HERE ?></textarea>
        </div>

        <div class="form-group">
            <label for="acronymTags">Tags <br>
                <small class="text-danger">Separate each tag with a Comma and a
                    Space
                </small>
            </label>
            <input class="form-control" type="text" name="acronym_tags" id="acronym_name"
                   value="<?php //TODO: GET THE ACRONYM TAGS AND DISPLAY HERE ?>"
                   autocomplete="off"/>
        </div>

        <input type="hidden" name="token" value="<?php echo Token::generate(); ?>"/>
        <button class="btn btn-primary" type="submit" value="addAcronym">Save Changes</button>

    </form>
<?php

require_once DOC_ROOT . 'templates/footer.php';