<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Home | Acronymn');

$user = new User();
$messages = new Message();

// get the contents of the posted search box
$findMe = '';
if (isset($_POST) && isset($_POST['findThis']) && !isset($_POST['clear'])) {
    $findMe = trim($_POST['findThis']);
}

// TODO:  create new instance of the Acronym class

// TODO: find All acronyms with the contents of "findMe" listed in alphabetical order

// TODO: numberAcronyms is the count the number of acronyms found


// make sure the data returned is in an array for processing in the page
$data = [];

$data = [1, 2, 3]; //<-- TODO: REMOVE THIS LINE BEFORE WORKING ON THIS CODE

// TODO: if (acronymns->count() > 0) {
// TODO:    if (is_array(acronymns->data())) {
// TODO:        $data = acronymns->data();
// TODO:    } else {
// TODO:        $data = [acronymns->data()];
// TODO:     }
// TODO: }

require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Acronyms</h1>
                <p class="lead">List, Search and Edit</p>
                <p>
                    Select action by clicking on the appropriate icon:
                    <i class="fa fa-info-circle mx-1"></i> Details
                    <i class="fa fa-pencil-alt mx-1"></i> Edit
                    <i class="fa fa-minus-circle mx-1"></i> Delete
                </p>
                <p>
                    <a href="<?= BASE_URL; ?>acronym/create.php"
                       class="btn btn-outline-light my-1">Add New</a>
                </p>
                <p>This version by: <em>YOUR NAME HERE</em></p>
            </div>
            <div class="col-2">
                <a href="<?= BASE_URL ?>acronym/" class="text-light nav-link">
                    <i class="fas fa-book fa-7x mx-1 text-light"></i>
                </a>
            </div>
        </div>
    </div>
<?php

// START: Display error mmessages (This code is given)
if ($messages > '' && $messages->messageCount() > 0) {
    foreach ($messages as $error => $message) {
        ?>
        <p class="alert alert-<?= $messages->errorColour($error); ?> alert-dismissible">
            <span class="col-3"><i class="fas fa-exclamation-circle"></i>
                <strong><?= $error; ?></strong>
            </span>
            <span class="col-9"><?= $message; ?></span>
        </p>
        <?php
    } // end foreach
} // end if messages
$messages->clear();
// END: GIVEN CODE
?>

    <div class="row">
        <div class="col-8">
            <form class="form-inline my-2 my-lg-0" method="post">
                <input class="form-control mr-sm-2" type="search" placeholder="Search"
                       id="findThis" name="findThis" value="<?= $findMe; ?>">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" name="search" value="search">
                    <i class="fa fa-search"></i> Search
                </button>
                <button class="btn btn-outline-warning m-2 my-sm-0" type="submit" name="clear" value="clear">
                    <i class="fa fa-backward"></i> Clear
                </button>
            </form>
        </div>
        <div class="col-4">
            <p class="my-2 text-right">
                We found <strong><!--  TODO: Display Number of found acromnyms --></strong> acronyms
            </p>
        </div>
    </div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th class="border-0">
                Acronym
            </th>
            <th class="border-0">
                Actions
            </th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($data as $acronym) {
            ?>
            <tr>
                <td>
                    <form method="TODO: SELECT CORRECT GET OR POST"
                          action="TODO: SELECT CORRECT PHP FILE THAT RETRIEVES A SINGLE ACRONYM"
                          class="form-inline" name="details">
                        <input type="hidden" name="findThis" value="<?php // TODO: DISPLAY THE ACRONYM NAME  ;?>"/>
                        <button class="btn btn-link text-dark text-left col" type="submit" name="detail" value="detail">
                            <!--  TODO: display the ACRONYM NAME -->
                        </button>
                    </form>
                </td>
                <td>
                    <div class="row">
                        <form method="TODO: SELECT CORRECT GET OR POST"
                              action="TODO: SELECT CORRECT PHP FILE THAT RETRIEVES A SINGLE ACRONYM"
                              class="form-inline" name="details">
                            <input type="hidden" name="findThis" value="<?php //TODO: DISPLAY THE ACRONYM NAME ;?>"/>
                            <input type="hidden" name="findID" value="<?php //TODO: DISPLAY THE ACRONYM ID ;?>"/>
                            <button class="btn btn-info text-left mx-1" type="submit" name="detail" value="detail">
                                <i class="fa fa-info-circle"></i>
                            </button>
                        </form>

                        <form method="SELECT CORRECT GET OR POST"
                              action="SELECT CORRECT PHP FILE TO EDIT AN ACRONYM"
                              class="form-inline" name="details">
                            <input type="hidden" name="findThis" value="<?php //TODO: DISPLAY THE ACRONYM NAME ;?>"/>
                            <input type="hidden" name="findID" value="<?php //TODO: DISPLAY THE ACRONYM ID ;?>"/>
                            <button class="btn btn-warning text-left mx-1" type="submit" name="edit" value="edit">
                                <i class="fa fa-pencil-alt"></i>

                            </button>
                        </form>

                        <form method="SELECT CORRECT GET OR POST"
                              action="SELECT CORRECT PHP FILE TO DELETE AN ACRONYM"
                              class="form-inline" name="details">
                            <input type="hidden" name="findThis" value="<?php //TODO: DISPLAY THE ACRONYM NAME ;?>"/>
                            <input type="hidden" name="findID" value="<?php //TODO: DISPLAY THE ACRONYM ID ;?>"/>
                            <button class="btn btn-danger mx-1" type="submit" name="delete" value="delete">
                                <i class="fa fa-minus-circle"></i>
                            </button>
                        </form>

                    </div>
                </td>
            </tr>
            <?php
        } // end for each
        ?>
        </tbody>
    </table>
<?php

require_once DOC_ROOT . 'templates/footer.php';