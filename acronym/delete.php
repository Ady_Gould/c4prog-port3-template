<?php
// Include Core Initialization File
require_once __DIR__ . '/../core/init.php';

Session::put('title', 'Delete | Acronym');

$user = new User();

// if user is not logged in then send them back to the index page
if (!$user->isLoggedIn()) {
    $messages->clear();
    $messages->add('Warning', 'Sorry but you must be logged in to delete acronyms.');
    Redirect::to(BASE_URL . 'acronym/index.php');
}

// TODO: CORRECT THIS LINE TO Create a new Acronym instance
$acronyms = new Acronym();

if (Input::exists() && Input::get('findThis')) {
    $findMe = trim($_POST['findThis']);
    $findID = trim($_POST['findID']);

} elseif (Input::get('acronymID') && Input::get('deleteAcronym')) {
    // find the acronym requested
    $result = $acronyms->find((int)Input::get('acronymID'));

    // TODO: If the acronym belongs to the user, OR the user is a moderator or admin, then
    if ($user->hasPermission(['moderator', 'admin']) || $user->data()->id === $result->id) {

        // TODO: Write the line of code that will delete the acronym with the ID selected
        // <-- ADD THE LINE TO DELETE THE ACRONYM HERE

        $messages->add('Success', 'Acronym, ' . Input::get('deleteThis') . ' deleted from list');

    } else {
        $messages->add('Danger', 'You are not allowed to delete the acronym.');
    } // end if acronym belongs to suer, or iuser is admin/moderator
    header('Location: index.php');
} else {
    header('Location: index.php');
} // end if page was posted correctly

// TODO: MODIFY the line of code below so it FINDS the acronym with the ID in $findID
$acronyms->YOUR_CODE_HERE;

/// TODO: Create a method in the acronym class file that counts the number of acronyms that have been found
$numFound = $acronyms->count();

// make sure the data returned is in an array for processing in the page
$data = [];
if ($numFound > 0) {
    if (is_array($acronyms->data())) {
        $data = $acronyms->data();
    } else {
        $data = [$acronyms->data()];
    }
}

require_once DOC_ROOT . 'templates/header.php';
?>
    <div class="jumbotron bg-success text-light">
        <div class="row">
            <div class="col-10">
                <h1>Delete Acronym</h1>
                <p>This version by: <em>YOUR NAME HERE</em></p></div>
            <div class="col-2">
                <i class="fas fa-book fa-7x mx-1 text-light"></i>
            </div>
        </div>
    </div>
<?php

?>
    <div class="row">
        <form action="delete.php" method="post" class="col-12">
            <h2 class="">Are you sure you wish to delete the acronym <strong class="text-primary"><?=
                    $data[0]->acronym_name; ?></strong>?</h2>
            <input class="form-control mr-sm-2" type="hidden" id="acronymID" name="acronymID" value="<?= $data[0]->id;
            ?>">
            <input class="form-control mr-sm-2" type="hidden" id="deleteThis" name="deleteThis"
                   value="<?= $data[0]->acronym_name; ?>">

            <button class="btn btn-danger m-2" name="deleteAcronym" value="Delete">
                <i class="fa fa-minus-circle mx-1"></i>
                Yes, Delete
            </button>
            <button class="btn btn-primary m-2" name="cancelDelete" value="Cancel">
                <i class="fa fa-times-circle mx-1"></i>
                No, Cancel
            </button>
        </form>
    </div>
<?php

require_once DOC_ROOT . 'templates/footer.php';